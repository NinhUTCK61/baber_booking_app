import { BeautyServiceHeader } from '@/components/BeautyServiceHeader'
import { Card } from '@/components/Card'
import { PillFilter } from '@/components/PillFilter'
import { SafeAreaView, ScrollView, Text, View } from 'react-native'

const ImageIntro = require('@/assets/images/bgBeautyService.png')

const popularNearYou = [
  {
    id: '1',
    img: '@/assets/images/beautyService.png',
    for: 'FOR MEN & WOMEN',
    title: 'Woodlands Hills Salon',
    service: 'Haircut, Spa, Massage',
    star: '4.5',
    locate: 'Keira throughway',
    distance: '2.5 km',
    price: '$50',
  },
  {
    id: '2',
    img: '@/assets/images/beautyService.png',
    for: 'FOR MEN & WOMEN',
    title: 'Woodlands Hills Salon',
    service: 'Haircut, Spa, Massage',
    star: '4.5',
    locate: 'Keira throughway',
    distance: '2.5 km',
    price: '$50',
  },
  {
    id: '3',
    img: '@/assets/images/beautyService.png',
    for: 'FOR MEN & WOMEN',
    title: 'Woodlands Hills Salon',
    service: 'Haircut, Spa, Massage',
    star: '4.5',
    locate: 'Keira throughway',
    distance: '2.5 km',
    price: '$50',
  },
  {
    id: '4',
    img: '@/assets/images/beautyService.png',
    for: 'FOR MEN & WOMEN',
    title: 'Woodlands Hills Salon',
    service: 'Haircut, Spa, Massage',
    star: '4.5',
    locate: 'Keira throughway',
    distance: '2.5 km',
    price: '$50',
  },
  {
    id: '5',
    img: '@/assets/images/beautyService.png',
    for: 'FOR MEN & WOMEN',
    title: 'Woodlands Hills Salon',
    service: 'Haircut, Spa, Massage',
    star: '4.5',
    locate: 'Keira throughway',
    distance: '2.5 km',
    price: '$50',
  },
]

export default function BeautyService() {
  return (
    <>
      <SafeAreaView className="flex-1 bg-white">
        <ScrollView showsVerticalScrollIndicator={false} className="bg-white">
          <BeautyServiceHeader ImageIntro={ImageIntro} title="Haircut for Men" />
          <View className="px-4 pt-4">
            <PillFilter />
          </View>
          <Text className="text-black text-base font-bold p-6">
            102 shops giving Haircut service
          </Text>
          <View className="flex flex-col gap-y-6 mx-6">
            {popularNearYou.map((service, index) => (
              <View key={index}>
                <Card key={index} {...service} />
              </View>
            ))}
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  )
}
