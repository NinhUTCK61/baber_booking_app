import BackIcon from '@/assets/icons/back.svg'
import CodeCorrectIcon from '@/assets/icons/codeCorrect.svg'
import PaymentIcon from '@/assets/icons/payment.svg'
import { router } from 'expo-router'
import { useState } from 'react'
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native'

const stateAction = {
  default: 'containerNormal',
  active: 'containerActive',
  inactive: 'containerInActive',
}

export default function AddPayment() {
  const [active, setActive] = useState(stateAction.default)
  const handleActive = (value: string) => {
    if (value === '') setActive(stateAction.default)
    else setActive(stateAction.active)
  }

  return (
    <SafeAreaView className="flex-1 bg-white">
      <ScrollView className="flex-1 mx-6">
        <TouchableOpacity onPress={() => router.back()}>
          <BackIcon width={24} height={24} />
        </TouchableOpacity>
        <Text className="pt-1 text-2xl font-bold">Add a Card</Text>
        <View className="mt-7">
          <View>
            <Text className="pl-2">Name on card</Text>
            <TextInput
              placeholder="John doe"
              className="h-[48] border border-slate-400 rounded-lg px-4 mb-6"
            />
          </View>
          <Text className="pl-2">Card number</Text>
          <View
            style={{ ...style.containerNormal, ...style[active as keyof typeof style] }}
            className="mb-6"
          >
            <PaymentIcon width={32} height={24} />
            <TextInput
              placeholder="Enter Coupon Code"
              className="h-full w-[260] px-2"
              onChangeText={handleActive}
            />
            <CodeCorrectIcon width={24} height={24} />
          </View>
          <View className="flex-row justify-between">
            <View className="w-[155]">
              <Text className="pl-2">Exp. Date</Text>
              <TextInput
                placeholder="MM/DD"
                className="h-[48] border border-slate-400 rounded-lg px-4 mb-6"
              />
            </View>
            <View className="w-[155]">
              <Text className="pl-2">CVV</Text>
              <TextInput
                placeholder="MM/DD"
                className="h-[48] border border-slate-400 rounded-lg px-4 mb-6"
              />
            </View>
          </View>
        </View>
        <TouchableOpacity className=" flex-row justify-center items-center bg-[#6440FE] rounded-lg h-14 mt-8">
          <Text className="text-white text-base font-bold">Add Card</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  )
}

const style = StyleSheet.create({
  containerNormal: {
    borderWidth: 1,
    borderRadius: 8,
    borderColor: '#8F90A6',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 48,
    paddingRight: 4,
    paddingLeft: 4,
  },
  containerActive: {
    borderColor: '#05A660',
  },
  containerInActive: {
    borderColor: 'red',
  },
})
