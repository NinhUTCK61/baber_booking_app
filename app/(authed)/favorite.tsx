import CloseIcon from '@/assets/icons/close.svg'
import { HorizontalDot } from '@/components/HorizontalDot'
import { ItemFavorite } from '@/components/ItemFavorite'
import { router } from 'expo-router'
import { SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native'

export default function Favorite() {
  return (
    <SafeAreaView className="flex-1 bg-white">
      <ScrollView className="flex-1 mx-6">
        <View className="flex-1">
          <TouchableOpacity onPress={() => router.back()}>
            <CloseIcon width={24} height={24} />
          </TouchableOpacity>
          <Text className="pt-1 text-2xl font-bold">Your favorites</Text>
          <View className="mt-10">
            <ItemFavorite />
          </View>
          <View className="pt-5">
            <HorizontalDot />
          </View>
          <View className="mt-10">
            <ItemFavorite />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
