import AddMethodIcon from '@/assets/icons/addMethod.svg'
import CloseIcon from '@/assets/icons/close.svg'
import VisaIcon from '@/assets/icons/visa.svg'
import { router } from 'expo-router'
import { SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native'

export default function Payment() {
  return (
    <SafeAreaView className="flex-1 bg-white">
      <ScrollView className="flex-1 mx-6">
        <TouchableOpacity onPress={() => router.back()}>
          <CloseIcon width={24} height={24} />
        </TouchableOpacity>
        <Text className="pt-1 text-2xl font-bold">Payment methods</Text>
        <View className="mt-5">
          <View className="flex-row items-center justify-between">
            <View className="flex-row items-center space-x-4">
              <View>
                <VisaIcon width={32} height={24} />
              </View>
              <Text className="text-base font-bold">Visa</Text>
            </View>
            <TouchableOpacity>
              <Text className="text-sm font-bold text-red-600">Remove</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            className="flex-row items-center space-x-4 py-4"
            onPress={() => router.push('/addpayment')}
          >
            <AddMethodIcon width={32} height={24} />
            <Text className="text-base font-bold">Add payment method</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}
