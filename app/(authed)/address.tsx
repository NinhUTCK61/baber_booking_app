import CloseIcon from '@/assets/icons/close.svg'
import DirectionIcon from '@/assets/icons/direction.svg'
import LocalIcon from '@/assets/icons/localIcon.svg'
import SearchDark from '@/assets/icons/searchDark.svg'
import { Horizontal } from '@/components/Horizontal'
import { router } from 'expo-router'
import { SafeAreaView, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'

const address = [
  {
    name: 'Royal Ln. Mesa',
    address: '2464 Royal Ln. Mesa, New Jersey 45463',
  },
  {
    name: 'Royal Ln. Mesa',
    address: '2464 Royal Ln. Mesa, New Jersey 45463',
  },
  {
    name: 'Royal Ln. Mesa',
    address: '2464 Royal Ln. Mesa, New Jersey 45463',
  },
  {
    name: 'Royal Ln. Mesa',
    address: '2464 Royal Ln. Mesa, New Jersey 45463',
  },
]

export default function Address() {
  return (
    <SafeAreaView className="flex-1 bg-white">
      <View className="p-6">
        <View className="flex-row justify-between items-center">
          <TouchableOpacity onPress={router.back}>
            <CloseIcon width={24} height={24} />
          </TouchableOpacity>
          <Text className="text-2xl font-bold w-full pr-6 text-center">Your Address</Text>
        </View>
        <View className="mt-7">
          <View>
            <Text className="pl-2">Enter your address</Text>
            <View className="flex-row items-center justify-between w-full">
              <View className="flex-row items-center space-x-3 h-12 bg-white px-3 border border-[#C7C9D9] rounded-lg max-w-[271] w-full">
                <SearchDark width={24} height={24} />
                <TextInput placeholder="Shop name" className="h-full ml-2 " />
              </View>
              <View className="h-12 w-12 flex-row items-center shadow-sm bg-white rounded-lg justify-center">
                <DirectionIcon width={17} height={17} />
              </View>
            </View>
          </View>
        </View>
        <View className="mt-6">
          <ScrollView className="h-full" showsVerticalScrollIndicator={false}>
            {address.map((item, index) => (
              <>
                <View key={index} className="flex-row items-center">
                  <LocalIcon width={24} height={24} />
                  <View className="pl-2">
                    <Text className="text-base">{item.name}</Text>
                    <Text className="text-[#8F90A6]">{item.address}</Text>
                  </View>
                </View>
                <Horizontal />
              </>
            ))}
          </ScrollView>
        </View>
      </View>
    </SafeAreaView>
  )
}
