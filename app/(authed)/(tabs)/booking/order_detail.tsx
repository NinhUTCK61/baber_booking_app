import CloseIcon from '@/assets/icons/close.svg'
import DateTimeIcon from '@/assets/icons/dateTime.svg'
import { Horizontal } from '@/components/Horizontal'
import { HorizontalDot } from '@/components/HorizontalDot'
import { router } from 'expo-router'
import { Text, TouchableOpacity, View } from 'react-native'

export default function OrderDetail() {
  return (
    <View className="flex-1">
      <View>
        <TouchableOpacity onPress={() => router.back()}>
          <CloseIcon width={24} height={24} />
        </TouchableOpacity>
        <Text className="pt-1 text-2xl font-bold">Order Details</Text>
        <Text className="pt-7 text-xl font-bold">Woodlands Hills Salon</Text>
        <TouchableOpacity className="flex-row items-center justify-between py-5 pt-10">
          <View className="flex-row items-center space-x-2">
            <DateTimeIcon width={24} height={24} />
            <Text className="text-base font-bold">10 March 2021</Text>
          </View>
        </TouchableOpacity>
        <View className="pt-2">
          <HorizontalDot />
        </View>
        <View className="pt-5 flex-row items-start justify-between">
          <View>
            <Text className="text-base font-bold">Haircut</Text>
            <View className="flex-row items-center space-x-1 mt-1">
              <View className="bg-[#E3FFF1] border border-[#05A660] rounded-md w-6 h-6 justify-center items-center">
                <Text className="text-[#05A660] text-[10px] font-bold">10</Text>
              </View>
              <Text>x</Text>
              <Text className="text-base">$160</Text>
            </View>
          </View>
          <View>
            <Text>$160</Text>
          </View>
        </View>
        <View className="py-4">
          <HorizontalDot />
        </View>
        <View className="flex-row justify-between items-center">
          <Text className="text-base">Item total</Text>
          <Text className="text-base">$112</Text>
        </View>
        <View className="flex-row justify-between items-center pt-3">
          <Text className="text-base">Coupon Discount</Text>
          <Text className="text-base text-[#05A660]">-$10</Text>
        </View>
        <Horizontal />
        <View className="flex-row justify-between items-center ">
          <Text className="text-xl font-bold">Grand Total</Text>
          <Text className="text-xl font-bold">$30</Text>
        </View>
      </View>
      <TouchableOpacity className=" flex-row justify-center items-center bg-[#6440FE] rounded-lg h-14 mt-8">
        <Text className="text-white text-base font-bold">Reorder Booking</Text>
      </TouchableOpacity>
    </View>
  )
}
