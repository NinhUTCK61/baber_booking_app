import { HorizontalDot } from '@/components/HorizontalDot'
import { ItemBooking } from '@/components/ItemBooking'
import { Text, View } from 'react-native'

export default function Booking() {
  return (
    <View>
      <Text className="pt-1 text-2xl font-bold">Your Bookings</Text>
      <View className="pt-10">
        <ItemBooking />
      </View>
      <View className="pt-5">
        <HorizontalDot />
      </View>
    </View>
  )
}
