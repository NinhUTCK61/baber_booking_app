import { Slot } from 'expo-router'
import { SafeAreaView, ScrollView } from 'react-native'

export default function Layout() {
  return (
    <SafeAreaView className="flex-1">
      <ScrollView className="flex-1 px-6">
        <Slot />
      </ScrollView>
    </SafeAreaView>
  )
}
