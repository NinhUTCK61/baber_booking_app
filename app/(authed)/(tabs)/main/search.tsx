import SearchHistoryIcon from '@/assets/icons/searchHistory.svg'
import { Card } from '@/components/Card'
import { Horizontal } from '@/components/Horizontal'
import { router, usePathname } from 'expo-router'
import { Text, TouchableOpacity, View } from 'react-native'
import { ItemService } from '../../../../components/ItemService'

const history = ['Haircut', 'Shave', 'Facial', 'Bleach', 'Waxing']
const popularNearYou = [
  {
    id: '1',
    img: '@/assets/images/beautyService.png',
    for: 'FOR MEN & WOMEN',
    title: 'Woodlands Hills Salon',
    service: 'Haircut, Spa, Massage',
    star: '4.5',
    locate: 'Keira throughway',
    distance: '2.5 km',
    price: '$50',
  },
  {
    id: '2',
    img: '@/assets/images/beautyService.png',
    for: 'FOR MEN & WOMEN',
    title: 'Woodlands Hills Salon',
    service: 'Haircut, Spa, Massage',
    star: '4.5',
    locate: 'Keira throughway',
    distance: '2.5 km',
    price: '$50',
  },
]

const beautyServices = [
  {
    id: '1',
    image: '@/assets/images/beautyService.png',
    title: 'Haircut for men',
  },
  {
    id: '2',
    image: '@/assets/images/beautyService.png',
    title: 'Shave for men',
  },
  {
    id: '3',
    image: '@/assets/images/beautyService.png',
    title: 'Facial for women',
  },
]

export default function Search() {
  const pathname = usePathname()
  const navigateToBeautyService = (id: string) => {
    router.push(`/beauty_service/${id}`)
  }

  return (
    <View className="px-6 w-full h-full bg-white">
      <View>
        <View className="flex-row justify-between">
          <Text className="text-base font-bold">Recently searched</Text>
          <Text className="text-[#6440FE]">Clear all</Text>
        </View>
        <View className="pt-3.5">
          {history.map((item, index) => (
            <View key={index} className="flex-row items-center">
              <SearchHistoryIcon width={24} height={24} color="#ada4a4" />
              <Text className="pl-3 text-base font-bold text-[#ada4a4]">{item}</Text>
            </View>
          ))}
        </View>
      </View>
      <Horizontal />
      <View>
        <Text className="text-base font-bold">Trending near you</Text>
        <View className="pt-3.5 space-y-6">
          {popularNearYou.map((service, index) => (
            <View key={index}>
              <Card key={index} {...service} />
            </View>
          ))}
        </View>
      </View>
      <Horizontal />
      <View>
        <Text className="text-base font-bold">Try these services</Text>
        <View className="flex-row flex-wrap justify-between">
          {beautyServices.map((service, index) => (
            <TouchableOpacity onPress={() => navigateToBeautyService(service.id)} key={service.id}>
              <ItemService key={index} image={service.image} title={service.title} />
            </TouchableOpacity>
          ))}
        </View>
      </View>
    </View>
  )
}
