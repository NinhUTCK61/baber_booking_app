import { HomeHeader } from '@/components/HomeHeader'
import { Slot } from 'expo-router'
import { SafeAreaView } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'

export default function Layout() {
  return (
    <SafeAreaView className="flex-1 bg-white">
      <ScrollView showsVerticalScrollIndicator={false}>
        <HomeHeader />
        <Slot />
      </ScrollView>
    </SafeAreaView>
  )
}
