import ArrowRightIcon from '@/assets/icons/arrowRightIcon.svg'
import { Card } from '@/components/Card'
import { ItemService } from '@/components/ItemService'
import { PillFilter } from '@/components/PillFilter'
import { router, usePathname } from 'expo-router'
import { ScrollView, Text, TouchableOpacity, View } from 'react-native'

const beautyServices = [
  {
    id: '1',
    image: '@/assets/images/beautyService.png',
    title: 'Haircut for men',
  },
  {
    id: '2',
    image: '@/assets/images/beautyService.png',
    title: 'Shave for men',
  },
  {
    id: '3',
    image: '@/assets/images/beautyService.png',
    title: 'Facial for women',
  },
  {
    id: '4',
    image: '@/assets/images/beautyService.png',
    title: 'Bleach for women',
  },
  {
    id: '5',
    image: '@/assets/images/beautyService.png',
    title: 'Waxing for women',
  },
  {
    id: '6',
    image: '@/assets/images/beautyService.png',
    title: 'Facial for women',
  },
]

const popularNearYou = [
  {
    id: '1',
    img: '@/assets/images/beautyService.png',
    for: 'FOR MEN & WOMEN',
    title: 'Woodlands Hills Salon',
    service: 'Haircut, Spa, Massage',
    star: '4.5',
    locate: 'Keira throughway',
    distance: '2.5 km',
    price: '$50',
  },
  {
    id: '2',
    img: '@/assets/images/beautyService.png',
    for: 'FOR MEN & WOMEN',
    title: 'Woodlands Hills Salon',
    service: 'Haircut, Spa, Massage',
    star: '4.5',
    locate: 'Keira throughway',
    distance: '2.5 km',
    price: '$50',
  },
  {
    id: '3',
    img: '@/assets/images/beautyService.png',
    for: 'FOR MEN & WOMEN',
    title: 'Woodlands Hills Salon',
    service: 'Haircut, Spa, Massage',
    star: '4.5',
    locate: 'Keira throughway',
    distance: '2.5 km',
    price: '$50',
  },
  {
    id: '4',
    img: '@/assets/images/beautyService.png',
    for: 'FOR MEN & WOMEN',
    title: 'Woodlands Hills Salon',
    service: 'Haircut, Spa, Massage',
    star: '4.5',
    locate: 'Keira throughway',
    distance: '2.5 km',
    price: '$50',
  },
  {
    id: '5',
    img: '@/assets/images/beautyService.png',
    for: 'FOR MEN & WOMEN',
    title: 'Woodlands Hills Salon',
    service: 'Haircut, Spa, Massage',
    star: '4.5',
    locate: 'Keira throughway',
    distance: '2.5 km',
    price: '$50',
  },
]

export default function Home() {
  const pathname = usePathname()
  const navigateToBeautyService = (id: string) => {
    router.push(`/beauty_service/${id}`)
  }

  const navigateToShop = (id: string) => {
    router.push(`/shop/${id}`)
  }
  return (
    <View className="w-full h-full bg-white">
      <View className="px-4 pb-4">
        <PillFilter />
      </View>

      <View className="mt-6 mx-6">
        <View className="flex-row items-center justify-between">
          <Text className="text-base font-bold">Beauty services</Text>
          <View className="flex-row items-center">
            <Text className="font-bold text-[#6440FE]">see all</Text>
            <ArrowRightIcon width={16} height={16} />
          </View>
        </View>
        <View className="flex-row flex-wrap justify-between">
          {beautyServices.map((service, index) => (
            <TouchableOpacity onPress={() => navigateToBeautyService(service.id)} key={service.id}>
              <ItemService key={index} image={service.image} title={service.title} />
            </TouchableOpacity>
          ))}
        </View>
      </View>
      <View className="mt-8">
        <View className="flex-row items-center justify-between mx-6">
          <Text className="text-base font-bold">Popular near you</Text>
          <View className="flex-row items-center">
            <Text className="font-bold text-[#6440FE]">see all</Text>
            <ArrowRightIcon width={16} height={16} />
          </View>
        </View>
        <ScrollView horizontal showsHorizontalScrollIndicator={false} className="mt-6">
          <View className="flex flex-row space-x-6 mx-6">
            {popularNearYou.map((service, index) => (
              <TouchableOpacity key={service.id} onPress={() => navigateToShop(service.id)}>
                <Card {...service} />
              </TouchableOpacity>
            ))}
          </View>
        </ScrollView>
      </View>
      <View className="mt-8">
        <View className="flex-row items-center justify-between mx-6">
          <Text className="text-base font-bold">Best Offers</Text>
          <View className="flex-row items-center">
            <Text className="font-bold text-[#6440FE]">see all</Text>
            <ArrowRightIcon width={16} height={16} />
          </View>
        </View>
        <ScrollView horizontal showsHorizontalScrollIndicator={false} className="mt-6">
          <View className="flex flex-row space-x-6 mx-6">
            {popularNearYou.map((service, index) => (
              <TouchableOpacity key={service.id} onPress={() => navigateToShop(service.id)}>
                <Card {...service} />
              </TouchableOpacity>
            ))}
          </View>
        </ScrollView>
      </View>
    </View>
  )
}
