import { Horizontal } from '@/components/Horizontal'
import { usePathname } from 'expo-router'
import { Image, Text, View } from 'react-native'

const shops = [
  {
    title: 'Shop 1',
    address: 'Address 1',
    distance: '1.5 km',
    image: '@/assets/images/beautyService.png',
  },
  {
    title: 'Shop 2',
    address: 'Address 2',
    distance: '2.5 km',
    image: '@/assets/images/beautyService.png',
  },
  {
    title: 'Shop 3',
    address: 'Address 3',
    distance: '3.5 km',
    image: '@/assets/images/beautyService.png',
  },
  {
    title: 'Shop 4',
    address: 'Address 4',
    distance: '4.5 km',
    image: '@/assets/images/beautyService.png',
  },
  {
    title: 'Shop 5',
    address: 'Address 5',
    distance: '5.5 km',
    image: '@/assets/images/beautyService.png',
  },
  {
    title: 'Shop 6',
    address: 'Address 6',
    distance: '6.5 km',
    image: '@/assets/images/beautyService.png',
  },
  {
    title: 'Shop 7',
    address: 'Address 7',
    distance: '7.5 km',
    image: '@/assets/images/beautyService.png',
  },
  {
    title: 'Shop 8',
    address: 'Address 8',
    distance: '8.5 km',
    image: '@/assets/images/beautyService.png',
  },
  {
    title: 'Shop 9',
    address: 'Address 9',
    distance: '9.5 km',
    image: '@/assets/images/beautyService.png',
  },
]

export default function Keyboard() {
  const pathname = usePathname()
  return (
    <View className="w-full h-full bg-white px-6">
      {shops.map((shop, index) => {
        return (
          <View key={index}>
            <View key={index} className="flex-row items-center space-x-3">
              <Image
                source={require('@/assets/images/beautyService.png')}
                className="rounded-full w-[60] h-[60]"
              />
              <View>
                <Text className="text-base font-bold truncate line-clamp-1">{shop.title}</Text>
                <Text className="text-[16] text-[#8F90A6] truncate line-clamp-1">
                  {shop.address}
                </Text>
                <Text className="text-[16] text-[#8F90A6] truncate line-clamp-1">
                  {shop.distance}
                </Text>
              </View>
            </View>
            <Horizontal />
          </View>
        )
      })}
    </View>
  )
}
