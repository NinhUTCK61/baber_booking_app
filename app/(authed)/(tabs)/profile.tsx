import ArrowRightDarkIcon from '@/assets/icons/arrowRightDark.svg'
import ArrowRightRed from '@/assets/icons/arrowRightRed.svg'
import HeartIcon from '@/assets/icons/heart.svg'
import LogoutIcon from '@/assets/icons/logout.svg'
import MgAddressIcon from '@/assets/icons/mgAddress.svg'
import PaymentIcon from '@/assets/icons/payment.svg'
import { Horizontal } from '@/components/Horizontal'
import Logout from '@/components/Logout'
import { router } from 'expo-router'
import { useState } from 'react'
import { Image, SafeAreaView, Text, TouchableOpacity, View } from 'react-native'

export default function Profile() {
  const [isLogout, setIsLogout] = useState(false)
  return (
    <>
      <SafeAreaView className="flex-1 bg-white">
        <View className="mx-6">
          <View className="flex-row items-center space-x-2 pb-2">
            <Image
              source={require('@/assets/images/profile.png')}
              className="w-[54] h-[54] rounded-full"
            />
            <Text className="text-base font-bold">John Doe</Text>
          </View>
          <Horizontal />

          <TouchableOpacity
            className="pt-8 flex-row items-start space-x-2"
            onPress={() => router.push('/favorite')}
          >
            <HeartIcon width={24} height={24} />
            <View className="w-full pr-8 flex-row items-center justify-between">
              <View>
                <Text className="text-base font-bold">Your favorites</Text>
                <Text className="text-xs text-[#8F90A6]">
                  Reorder your favorite service in a click
                </Text>
              </View>
              <ArrowRightDarkIcon width={16} height={16} />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            className="pt-8 flex-row items-start space-x-2"
            onPress={() => router.push('/payment')}
          >
            <PaymentIcon width={24} height={24} />
            <View className="w-full pr-8 flex-row items-center justify-between">
              <View>
                <Text className="text-base font-bold">Payments</Text>
                <Text className="text-xs text-[#8F90A6]">Payment methods, Transaction History</Text>
              </View>
              <ArrowRightDarkIcon width={16} height={16} />
            </View>
          </TouchableOpacity>

          <TouchableOpacity className="pt-8 flex-row items-start space-x-2">
            <MgAddressIcon width={24} height={24} />
            <View className="w-full pr-8 flex-row items-start justify-between">
              <Text className="text-base font-bold">Manage Address</Text>
              <ArrowRightDarkIcon width={16} height={16} />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            className="pt-8 flex-row items-start space-x-2"
            onPress={() => setIsLogout(true)}
          >
            <LogoutIcon width={24} height={24} />
            <View className="w-full pr-8 flex-row items-center justify-between">
              <Text className="text-base font-bold text-[#E53535]">Logout</Text>
              <ArrowRightRed width={16} height={16} />
            </View>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
      <Logout openBottomSheet={isLogout} setOpenBottomSheet={setIsLogout} />
    </>
  )
}
