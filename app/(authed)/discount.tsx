import BackIcon from '@/assets/icons/back.svg'
import CodeCorrectIcon from '@/assets/icons/codeCorrect.svg'
import { router } from 'expo-router'
import { useState } from 'react'
import { SafeAreaView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'

const stateAction = {
  default: 'containerNormal',
  active: 'containerActive',
  inactive: 'containerInActive',
}

export default function Discount() {
  const [active, setActive] = useState(stateAction.default)
  const handleActive = () => {
    setActive(stateAction.active)
  }
  return (
    <SafeAreaView className="flex-1">
      <View className="px-6 pb-2">
        <TouchableOpacity onPress={() => router.back()}>
          <BackIcon width={24} height={24} />
        </TouchableOpacity>
        <Text className="pt-1 text-2xl font-bold">Offer & Promo Codes</Text>
        <View className="mt-5 ">
          <Text className="text-xs pl-2">Coupon Code</Text>
          <View className="flex-row items-center justify-between space-x-4">
            <View style={{ ...style.containerNormal, ...style[active as keyof typeof style] }}>
              <TextInput placeholder="Enter Coupon Code" className="h-full w-full" />
              {active === stateAction.active && <CodeCorrectIcon width={24} height={24} />}
            </View>
            <TouchableOpacity onPress={handleActive}>
              <Text className="text-[#6440FE] font-bold uppercase">APPLY</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View className="h-4">
          {active === stateAction.active && (
            <View className="pl-2 flex-row items-center pt-1">
              <CodeCorrectIcon width={16} height={16} />
              <Text className="text-xs">Coupon Code is Valid. You will get 10% off on total</Text>
            </View>
          )}
        </View>
      </View>
    </SafeAreaView>
  )
}

const style = StyleSheet.create({
  containerNormal: {
    borderWidth: 1,
    borderRadius: 8,
    borderColor: '#8F90A6',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: 274,
    height: 48,
    paddingRight: 30,
    paddingLeft: 12,
  },
  containerActive: {
    borderColor: '#05A660',
  },
  containerInActive: {
    borderColor: 'red',
  },
})
