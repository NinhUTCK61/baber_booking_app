import BackIcon from '@/assets/icons/back.svg'
import DotIcon from '@/assets/icons/dot.svg'
import HeartIcon from '@/assets/icons/heart.svg'
import LocalIcon from '@/assets/icons/localIcon.svg'
import PercentIcon from '@/assets/icons/percentIcon.svg'
import PhoneIcon from '@/assets/icons/phone.svg'
import SearchDarkIcon from '@/assets/icons/searchDark.svg'
import ShareIcon from '@/assets/icons/share.svg'
import StarLightIcon from '@/assets/icons/starLight.svg'
import { Horizontal } from '@/components/Horizontal'
import { SelectItem } from '@/components/SelectItem'
import { ShopService } from '@/components/ShopService'
import { router } from 'expo-router'
import { SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native'

export default function Shop() {
  return (
    <>
      <SafeAreaView className="flex-1 bg-white">
        <ScrollView showsVerticalScrollIndicator={false} className="px-6">
          <View className="flex-row justify-between">
            <TouchableOpacity onPress={router.back}>
              <BackIcon width={24} height={24} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => router.push('/(authed)/(tabs)/main/search')}>
              <SearchDarkIcon width={24} height={24} />
            </TouchableOpacity>
          </View>
          <View className="mt-6 flex-row flex-nowrap items-start justify-between">
            <View>
              <Text
                numberOfLines={2}
                ellipsizeMode="tail"
                className=" text-black text-2xl font-bold"
              >
                Woodlands Hills Salon
              </Text>
              <View className="flex-row items-center mt-0.5">
                <Text numberOfLines={2} ellipsizeMode="tail" className="text-base text-[#8F90A6] ">
                  Haircut, Spa, Massage
                </Text>
              </View>
              <View className="flex-row items-center">
                <Text numberOfLines={2} ellipsizeMode="tail" className="text-base text-[#8F90A6] ">
                  Keira throughway
                </Text>
                <DotIcon width={4} height={4} className="px-2" />
                <Text numberOfLines={2} ellipsizeMode="tail" className="text-base text-[#8F90A6] ">
                  5.0 Kms
                </Text>
              </View>
            </View>
            <View className="flex-col items-center">
              <View className="h-[30px] flex-row items-center justify-center space-x-2 border rounded-lg border-[#0063F7] w-[60]">
                <StarLightIcon width={12} height={12} />
                <Text className="text-sm font-bold text-[#0063F7]">4.1</Text>
              </View>
              <Text className="text-xs mt-1 text-[#0063F7]">5k+ ratings</Text>
            </View>
          </View>
          <View className="mt-4 flex flex-row space-x-5">
            <View className="flex-col items-center space-y-2">
              <PhoneIcon width={24} height={24} />
              <Text className="text-xs text-black">Call</Text>
            </View>
            <View className="flex-col items-center space-y-2">
              <LocalIcon width={24} height={24} />
              <Text className="text-xs text-black">Directions</Text>
            </View>
            <View className="flex-col items-center space-y-2">
              <ShareIcon width={24} height={24} />
              <Text className="text-xs text-black">Share</Text>
            </View>
            <View className="flex-col items-center space-y-2">
              <HeartIcon width={24} height={24} />
              <Text className="text-xs text-black">Favorite</Text>
            </View>
          </View>
          <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            className="mt-7 h-[55px] space-x-3"
          >
            <View className="border rounded-lg flex-col items-center justify-center px-3 h-[55] border-[#F2F2F5]">
              <View className="flex-row items-center space-x-2">
                <PercentIcon width={20} height={22} />
                <Text className="font-bold">50% off</Text>
              </View>
              <Text className="text-xs">use code FREE50</Text>
            </View>
            <View className="border rounded-lg flex-col items-center justify-center px-3 h-[55] border-[#F2F2F5]">
              <View className="flex-row items-center space-x-2">
                <PercentIcon width={20} height={22} />
                <Text className="font-bold">60% off on Debit Card</Text>
              </View>
              <Text className="text-xs">No coupon required</Text>
            </View>
          </ScrollView>
          <View className="mt-4">
            <Text className="text-base font-bold">Recommended (5)</Text>
            <View className="pt-4">
              <ShopService />
            </View>
          </View>
          <Horizontal />
          <View>
            <Text className="text-base font-bold">Packages</Text>
            <View className="pt-4">
              <ShopService />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
      <SelectItem />
    </>
  )
}
