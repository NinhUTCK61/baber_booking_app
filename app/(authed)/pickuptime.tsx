import ArrowRightIcon from '@/assets/icons/arrowRightDark.svg'
import BackIcon from '@/assets/icons/back.svg'
import VisaIcon from '@/assets/icons/visa.svg'
import { HorizontalDot } from '@/components/HorizontalDot'
import { router } from 'expo-router'
import { useState } from 'react'
import { SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native'

const listTime = [
  {
    id: 1,
    date: '19',
    day: 'Mon',
  },
  {
    id: 2,
    date: '20',
    day: 'Tue',
  },
  {
    id: 3,
    date: '21',
    day: 'Wed',
  },
  {
    id: 4,
    date: '22',
    day: 'Thu',
  },
  {
    id: 5,
    date: '23',
    day: 'Fri',
  },
]
const times = [
  {
    id: 1,
    time: '8:00 AM',
  },
  {
    id: 2,
    time: '8:30 AM',
  },
  {
    id: 3,
    time: '9:00 AM',
  },
  {
    id: 4,
    time: '9:30 AM',
  },
  {
    id: 5,
    time: '10:00 PM',
  },
  {
    id: 6,
    time: '10:30 PM',
  },
  {
    id: 7,
    time: '14:00 PM',
  },
  {
    id: 8,
    time: '14:30 PM',
  },
  {
    id: 9,
    time: '15:00 PM',
  },
  {
    id: 10,
    time: '15:30 PM',
  },
  {
    id: 11,
    time: '16:00 PM',
  },
  {
    id: 12,
    time: '16:30 PM',
  },
]

export default function PickUpTime() {
  const [active, setActive] = useState(0)
  const [timePick, setTimePick] = useState(0)
  return (
    <>
      <SafeAreaView className="flex-1 bg-white">
        <ScrollView className="px-6 flex-1">
          <TouchableOpacity onPress={() => router.back()}>
            <BackIcon width={24} height={24} />
          </TouchableOpacity>
          <Text className="pt-1 text-2xl font-bold">Checkout</Text>
          <Text className="pt-6 text-xl font-bold">Select Date & Time for the appointment</Text>
          <Text className="pt-6 text-base font-bold">When would you like your service?</Text>
          <View className="flex-row items-center space-x-4 mt-2">
            {listTime.map((time, index) => (
              <TouchableOpacity
                key={index}
                className="flex-col items-center justify-center w-12 h-12 border rounded-md"
                style={{
                  borderColor: active === time.id ? '#6440FE' : '#C7C9D9',
                }}
                onPress={() => setActive(time.id)}
              >
                <Text
                  className="text-sm"
                  style={{ color: active === time.id ? '#6440FE' : '#000' }}
                >
                  {time.day}
                </Text>
                <Text
                  className="text-base font-bold"
                  style={{ color: active === time.id ? '#6440FE' : '#000' }}
                >
                  {time.date}
                </Text>
              </TouchableOpacity>
            ))}
          </View>
          <Text className="pt-6 text-base font-bold">When would you like your service?</Text>
          <View className="flex flex-row flex-wrap items-center justify-around w-full space-y-4">
            {times.map((time, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => setTimePick(time.id)}
                className="flex-row items-center justify-center h-12 border rounded-md w-full max-w-[155]"
                style={{ borderColor: timePick === time.id ? '#6440FE' : '#C7C9D9' }}
              >
                <Text
                  className="text-base"
                  style={{ color: timePick === time.id ? '#6440FE' : '#000' }}
                >
                  {time.time}
                </Text>
              </TouchableOpacity>
            ))}
          </View>
          <View className="pt-6">
            <HorizontalDot />
          </View>
          <TouchableOpacity className="flex-row items-center justify-between py-5">
            <View className="flex-row items-center space-x-4">
              <VisaIcon width={32} height={24} />
              <Text className="font-bold">4153 xxxx xxxxx 0981</Text>
            </View>
            <ArrowRightIcon width={16} height={16} />
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    </>
  )
}
