import ArrowRightIcon from '@/assets/icons/arrowRight.svg'
import BackIcon from '@/assets/icons/back.svg'
import DateTimeIcon from '@/assets/icons/dateTime.svg'
import { Horizontal } from '@/components/Horizontal'
import { HorizontalDot } from '@/components/HorizontalDot'
import { InputNumber } from '@/components/InputNumber'
import { PromoCode } from '@/components/PromoCode'
import { router } from 'expo-router'
import { SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native'

export default function Checkout() {
  return (
    <SafeAreaView className="flex-1">
      <ScrollView className="px-6 flex-1">
        <TouchableOpacity onPress={() => router.back()}>
          <BackIcon width={24} height={24} />
        </TouchableOpacity>
        <Text className="pt-1 text-2xl font-bold">Checkout</Text>
        <Text className="pt-7 text-xl font-bold">Woodlands Hills Salon</Text>
        <TouchableOpacity
          className="flex-row items-center justify-between py-5"
          onPress={() => router.push('/pickuptime')}
        >
          <View className="flex-row items-center space-x-2">
            <DateTimeIcon width={24} height={24} />
            <Text className="text-base font-bold">Select Date & Time</Text>
          </View>
          <ArrowRightIcon width={16} height={16} />
        </TouchableOpacity>
        <View className="pt-2">
          <HorizontalDot />
        </View>
        <View className="pt-5 flex-row items-start justify-between">
          <View>
            <Text className="text-base font-bold">Haircut</Text>
            <Text className="text-base">$160</Text>
          </View>
          <View className="flex-col items-end">
            <InputNumber />
            <Text className="pt-1">$160</Text>
          </View>
        </View>
        <View className="py-4">
          <HorizontalDot />
        </View>
        <PromoCode />
        <View className="py-4">
          <HorizontalDot />
        </View>
        <View className="flex-row justify-between items-center pt-4">
          <Text className="text-base">Item total</Text>
          <Text className="text-base">$112</Text>
        </View>
        <View className="flex-row justify-between items-center pt-3">
          <Text className="text-base">Coupon Discount</Text>
          <Text className="text-base text-[#05A660]">-$10</Text>
        </View>
        <Horizontal />
        <View className="flex-row justify-between items-center ">
          <Text className="text-xl font-bold">Amount payable</Text>
          <Text className="text-xl font-bold">$30</Text>
        </View>
      </ScrollView>
      <TouchableOpacity className="mx-6 flex-row justify-center items-center bg-[#6440FE] rounded-lg h-14">
        <Text className="text-white text-base font-bold">Payment</Text>
      </TouchableOpacity>
    </SafeAreaView>
  )
}
