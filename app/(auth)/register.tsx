import { router } from 'expo-router'
import { Text, TextInput, TouchableOpacity, View } from 'react-native'

export default function Register() {
  return (
    <View className="w-full h-full">
      <View className="h-[189] flex justify-end bg-[#6440FE] px-6 pb-7">
        <Text className="font-bold text-3xl text-white">SignUp</Text>
        <View className="flex-row pt-4">
          <Text className="text-white text-base pr-1">Already have an Account?</Text>
          <Text onPress={() => router.push('/login')} className="text-white text-base underline">
            Login
          </Text>
        </View>
      </View>
      <View className="p-6 flex-col gap-y-4">
        <View>
          <Text className="pl-2">User name</Text>
          <TextInput
            placeholder="User name"
            className="h-[48] border border-slate-400 rounded-lg px-4"
          />
        </View>
        <View>
          <Text className="pl-2">Email</Text>
          <TextInput
            placeholder="Email"
            className="h-[48] border border-slate-400 rounded-lg px-4"
          />
        </View>
        <View>
          <Text className="pl-2">Password</Text>
          <TextInput
            placeholder="Password"
            className="h-[48] border border-slate-400 rounded-lg px-4"
          />
        </View>
        <TouchableOpacity
          onPress={() => router.push('/login')}
          className=" w-full h-12 bg-[#6440FE] items-center justify-center border rounded-lg border-[#6440FE] border-solid"
        >
          <Text className="text-white font-bold">SignUp</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
