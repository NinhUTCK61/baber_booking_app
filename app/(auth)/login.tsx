import { router } from 'expo-router'
import { Text, TextInput, TouchableOpacity, View } from 'react-native'

export default function Login() {
  return (
    <View className="w-full h-full">
      <View className="h-[225] flex justify-end px-6 pb-7">
        <Text className="text-black font-bold text-3xl">Welcome Back!</Text>
        <View className="flex-row pt-4">
          <Text className="text-black text-base pr-1">Don’t have an account?</Text>
          <Text
            onPress={() => router.push('/register')}
            className="text-[#6440FE] text-base underline"
          >
            SignUp
          </Text>
        </View>
      </View>
      <View className="p-6 pt-10 flex-col gap-y-4">
        <View>
          <Text className="pl-2">Email</Text>
          <TextInput
            placeholder="Email"
            className="h-[48] border border-slate-400 rounded-lg px-4"
          />
        </View>
        <View>
          <Text className="pl-2">Password</Text>
          <TextInput
            placeholder="Password"
            className="h-[48] border border-slate-400 rounded-lg px-4"
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            router.replace('/(authed)/(tabs)/main')
          }}
          className=" w-full h-12 bg-[#6440FE] items-center justify-center border rounded-lg border-[#6440FE] border-solid mt-6"
        >
          <Text className="text-white font-bold">Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
