import { router } from 'expo-router'
import { ImageBackground, Text, TouchableOpacity, View } from 'react-native'
const ImageIntro = require('@/assets/images/bg_intro.jpg')

export default function Intro() {
  return (
    <ImageBackground source={ImageIntro} className="flex-1">
      <View className="justify-end w-full h-full mb-[60]">
        <Text className="text-2xl font-bold text-white px-8">
          Schedule the Appointmentin the best Salon for you.
        </Text>
        <View className="flex-row flex-nowrap justify-between px-5 gap-x-5 mt-[50] mb-[60]">
          <TouchableOpacity
            onPress={() => router.push('/login')}
            className="max-w-[160] w-full h-12 items-center justify-center border rounded-lg border-white border-solid"
          >
            <Text className="text-white font-bold">Login</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => router.push('/register')}
            className="max-w-[160] w-full h-12 bg-white items-center justify-center border rounded-lg border-[#6440FE] border-solid"
          >
            <Text className="text-[#6440FE] font-bold">Register</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ImageBackground>
  )
}
