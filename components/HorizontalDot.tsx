import { View } from 'react-native'
import { Line, Svg } from 'react-native-svg'

export const HorizontalDot = () => {
  const dotSpacing = 3
  const dashArray = `${dotSpacing}, ${dotSpacing}`
  const height = 1
  const width = '100%'
  const color = '#000'
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Svg height={height} width={width} style={{ alignSelf: 'center' }}>
        <Line
          stroke={color}
          strokeWidth={width}
          strokeDasharray={dashArray}
          x1="0"
          y1="0"
          x2={width}
          y2={height}
        />
      </Svg>
    </View>
  )
}
