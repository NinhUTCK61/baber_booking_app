import ArrowDownIcon from '@/assets/icons/arrowDownIcon.svg'
import { Text, View } from 'react-native'

const pillArray = ['Gender', 'Price', 'Offer']

export const PillFilter = () => {
  return (
    <View className="flex flex-row gap-x-2">
      {pillArray.map((text, index) => (
        <View
          key={index}
          className="flex-row items-center bg-[#F2F2F5] h-[25] w-fit px-3 rounded-full mr-3"
        >
          <Text className="pr-1 text-[17]">{text}</Text>
          <ArrowDownIcon width={16} height={16} />
        </View>
      ))}
    </View>
  )
}
