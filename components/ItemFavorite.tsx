import DotIcon from '@/assets/icons/dot.svg'
import FavoriteIcon from '@/assets/icons/favorite.svg'
import { Text, TouchableOpacity, View } from 'react-native'

export const ItemFavorite = () => {
  return (
    <View>
      <View className="flex-row justify-between">
        <View>
          <Text className="text-base font-bold">Woodlands Hills Salon</Text>
          <View className="flex-row items-center space-x-0 pt-1">
            <Text className="text-sm text-[#8F90A6]">Keira throughway</Text>
            <DotIcon width={4} height={4} className="px-3" />
            <Text className="text-sm text-[#8F90A6]">5.0 Kms</Text>
          </View>
          <Text className="text-sm text-[#8F90A6]">Haircut x 1 + Shave x 1</Text>
          <View className="flex-row items-center space-x-0 pt-1">
            <Text className="text-sm text-[#8F90A6]">12 Mar 2021</Text>
            <DotIcon width={4} height={4} className="px-3" />
            <Text className="text-sm text-[#8F90A6]">8:30 pm</Text>
            <DotIcon width={4} height={4} className="px-3" />
            <Text className="text-sm text-[#8F90A6]">$102</Text>
          </View>
        </View>
        <FavoriteIcon width={24} height={24} />
      </View>
      <View className="flex-row justify-end mt-5">
        <TouchableOpacity className="border border-[#6440FE] h-8 px-4 rounded-lg flex-row items-center">
          <Text className="font-bold text-[#6440FE]">Reorder Booking</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
