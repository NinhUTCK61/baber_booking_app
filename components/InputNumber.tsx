import MinusIcon from '@/assets/icons/minus.svg'
import PlusNumberIcon from '@/assets/icons/plusNumber.svg'
import React, { useState } from 'react'
import { TextInput, TouchableOpacity, View } from 'react-native'

export const InputNumber = () => {
  const [value, setValue] = useState(0)

  const increment = () => {
    setValue((prevValue) => prevValue + 1)
  }

  const decrement = () => {
    setValue((prevValue) => prevValue - 1)
  }

  const handleChange = (text: string) => {
    const number = parseInt(text, 10)
    if (isNaN(number) || number < 0) {
      setValue(0)
    } else {
      setValue(number)
    }
  }

  return (
    <View className="flex-row items-center justify-between w-[73] h-[31] border-[#6440FE] border rounded-lg px-1">
      <TouchableOpacity onPress={decrement}>
        <MinusIcon height={16} width={16} />
      </TouchableOpacity>
      <TextInput
        keyboardType="numeric"
        value={value.toString()}
        onChangeText={handleChange}
        className="text-center"
      />
      <TouchableOpacity onPress={increment} disabled={Number(value) >= 10 ? true : false}>
        <PlusNumberIcon height={16} width={16} />
      </TouchableOpacity>
    </View>
  )
}
