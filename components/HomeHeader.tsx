import { router, usePathname } from 'expo-router'
import { TextInput, TouchableOpacity, View } from 'react-native'
import CloseIcon from '../assets/icons/close.svg'
import SearchIcon from '../assets/icons/searchIcon.svg'
import { Locate } from './Locate'

const HomeHeader = () => {
  const pathname = usePathname()

  const onFocus = () => {
    router.push('/(authed)/(tabs)/main/search')
  }

  const onChange = (value: string) => {
    if (value && pathname === '/main/search') return router.push('/main/keyboard')
    if (!value && pathname === '/main/keyboard') {
      router.back()
    }
  }

  const segmentCondition = pathname === '/main/keyboard' || pathname === '/main/search'

  return (
    <View className="w-full px-6 pb-4">
      <View className="flex-row justify-between">
        <Locate type="dark" />
        {pathname === '/main/search' && (
          <TouchableOpacity onPress={router.back}>
            <CloseIcon width={24} height={24} />
          </TouchableOpacity>
        )}
      </View>
      <View className="flex-row items-center mt-4 shadow-sm bg-white h-[39] px-3 rounded-lg ">
        <SearchIcon width={16} height={16} />
        <TextInput
          autoFocus={segmentCondition}
          placeholder="Shop name"
          className="h-full w-full ml-2 "
          onFocus={onFocus}
          onChangeText={onChange}
        />
      </View>
    </View>
  )
}

export { HomeHeader }
