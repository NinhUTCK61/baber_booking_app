import { Image, Text, View } from 'react-native'

type ItemServiceProps = {
  image: string
  title: string
}

export const ItemService = (props: ItemServiceProps) => {
  return (
    <View className="w-[95] flex-col items-center gap-y-1.5 mt-4">
      <Image
        source={require('@/assets/images/beautyService.png')}
        className="w-[88] h-[88] rounded-full"
      />
      <Text numberOfLines={2} ellipsizeMode="tail" className=" text-center text-black">
        {props.title}
      </Text>
    </View>
  )
}
