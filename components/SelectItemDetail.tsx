import BottomSheet, {
  BottomSheetBackdrop,
  BottomSheetBackdropProps,
  BottomSheetView,
} from '@gorhom/bottom-sheet'
import React, { useCallback, useRef } from 'react'
import { ScrollView, StyleSheet, View } from 'react-native'
import { ItemPickUp } from './ItemPickUp'

type SelectItemDetailProps = {
  openBottomSheet: boolean
  setOpenBottomSheet: (open: boolean) => void
}

const SelectItemDetail = (props: SelectItemDetailProps) => {
  const { openBottomSheet, setOpenBottomSheet } = props
  const bottomSheetRef = useRef<BottomSheet>(null)

  const handleSheetChanges = (index: number) => {
    if (index === -1) {
      setOpenBottomSheet(false)
    }
  }

  const renderBackdrop = useCallback(
    (props: React.PropsWithChildren<BottomSheetBackdropProps>) => (
      <BottomSheetBackdrop {...props} disappearsOnIndex={-1} appearsOnIndex={0} />
    ),
    [],
  )

  return (
    <BottomSheet
      snapPoints={['60%']}
      index={openBottomSheet ? 0 : -1}
      ref={bottomSheetRef}
      onChange={handleSheetChanges}
      enablePanDownToClose={true}
      bottomInset={80}
      backdropComponent={renderBackdrop}
    >
      <BottomSheetView style={styles.contentContainer}>
        <ScrollView className="w-full px-6">
          <View className="pt-4">
            <ItemPickUp />
          </View>
        </ScrollView>
      </BottomSheetView>
    </BottomSheet>
  )
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center',
  },
})

export default SelectItemDetail
