import { router } from 'expo-router'
import { ImageBackground, Text, TouchableOpacity, View } from 'react-native'
import BackLight from '../assets/icons/backLight.svg'
import { Locate } from './Locate'

type BeautyServiceHeaderProps = {
  ImageIntro: any
  title: string
}

export const BeautyServiceHeader = (props: BeautyServiceHeaderProps) => {
  return (
    <ImageBackground source={props.ImageIntro} className="h-[236] px-5 pb-5 ">
      <View className="pt-14 flex-col gap-y-6">
        <View className="flex flex-row">
          <TouchableOpacity onPress={router.back}>
            <BackLight width={24} height={24} />
          </TouchableOpacity>
          <View className="flex-row justify-center w-full pr-6">
            <Locate type="light" />
          </View>
        </View>
        <Text className="text-5xl font-bold text-white">{props.title}</Text>
      </View>
    </ImageBackground>
  )
}
