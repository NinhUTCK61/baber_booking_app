import CloseIcon from '@/assets/icons/close.svg'
import BottomSheet, {
  BottomSheetBackdrop,
  BottomSheetBackdropProps,
  BottomSheetView,
} from '@gorhom/bottom-sheet'
import React, { useCallback, useRef } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

type LogoutProps = {
  openBottomSheet: boolean
  setOpenBottomSheet: (open: boolean) => void
}

const Logout = (props: LogoutProps) => {
  const { openBottomSheet, setOpenBottomSheet } = props
  const bottomSheetRef = useRef<BottomSheet>(null)

  const handleSheetChanges = (index: number) => {
    if (index === -1) {
      setOpenBottomSheet(false)
    }
  }

  const renderBackdrop = useCallback(
    (props: React.PropsWithChildren<BottomSheetBackdropProps>) => (
      <BottomSheetBackdrop {...props} disappearsOnIndex={-1} appearsOnIndex={0} />
    ),
    [],
  )
  console.log('openBottomSheet', openBottomSheet)
  return (
    <BottomSheet
      snapPoints={[190]}
      index={openBottomSheet ? 0 : -1}
      ref={bottomSheetRef}
      onChange={handleSheetChanges}
      backdropComponent={renderBackdrop}
    >
      <BottomSheetView style={styles.contentContainer}>
        <View>
          <View className="flex-row items-start justify-between">
            <Text className="text-lg font-bold">Logout?</Text>
            <TouchableOpacity onPress={() => bottomSheetRef.current?.close()}>
              <CloseIcon width={24} height={24} />
            </TouchableOpacity>
          </View>
          <Text className="text-sm text-[#8F90A6]">Are you sure want to logout from the app?</Text>
          <View className="mt-8 flex-row justify-between">
            <TouchableOpacity
              className="flex-row items-center justify-center pt-4 bg-white py-2 px-4 h-12 w-[132] rounded-lg border border-[#6440FE]"
              onPress={() => bottomSheetRef.current?.close()}
            >
              <Text className="text-[#6440FE] text-sm font-bold">Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity
              className="flex-row items-center justify-center pt-4 bg-[#E53535] py-2 px-4 h-12 w-[184] rounded-lg"
              onPress={() => bottomSheetRef.current?.close()}
            >
              <Text className="text-white text-sm font-bold">Logout</Text>
            </TouchableOpacity>
          </View>
        </View>
      </BottomSheetView>
    </BottomSheet>
  )
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  contentContainer: {
    flex: 1,
    padding: 24,
    paddingTop: 16,
  },
})

export default Logout
