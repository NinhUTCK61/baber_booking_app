import DotIcon from '@/assets/icons/dot.svg'
import { router } from 'expo-router'
import { Text, TouchableOpacity, View } from 'react-native'

export const ItemBooking = () => {
  return (
    <View>
      <TouchableOpacity onPress={() => router.push('/booking/order_detail')}>
        <Text className="text-base font-bold">Woodlands Hills Salon</Text>
        <View className="flex-row items-center space-x-0 pt-1">
          <Text className="text-sm text-[#8F90A6]">Keira throughway</Text>
          <DotIcon width={4} height={4} className="px-3" />
          <Text className="text-sm text-[#8F90A6]">5.0 Kms</Text>
        </View>
        <Text className="text-sm text-[#8F90A6]">Haircut x 1 + Shave x 1</Text>
        <View className="flex-row items-center space-x-0 pt-1">
          <Text className="text-sm text-[#8F90A6]">12 Mar 2021</Text>
          <DotIcon width={4} height={4} className="px-3" />
          <Text className="text-sm text-[#8F90A6]">8:30 pm</Text>
          <DotIcon width={4} height={4} className="px-3" />
          <Text className="text-sm text-[#8F90A6]">$102</Text>
        </View>
      </TouchableOpacity>
      <View className="flex-row justify-between mt-4">
        <TouchableOpacity>
          <Text className="font-bold text-red-600">Cancel Booking</Text>
        </TouchableOpacity>
        <Text className="font-bold text-yellow-600">Pending</Text>
      </View>
    </View>
  )
}
