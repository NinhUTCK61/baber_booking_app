import RemoveItemIcon from '@/assets/icons/removeItem.svg'
import TimeIcon from '@/assets/icons/time.svg'
import { Image, Text, TouchableOpacity, View } from 'react-native'

export const ItemPickUp = () => {
  return (
    <View className="flex-row items-center justify-between ">
      <View className="flex-row items-start space-x-2">
        <Image
          source={require('@/assets/images/shopService.png')}
          className="h-[75] w-[75] rounded-2xl"
        />
        <View>
          <Text numberOfLines={2} ellipsizeMode="tail" className="font-bold w-[162px]">
            Casmara Brightening Facial
          </Text>
          <Text className="text-xs text-[#8F90A6]">$140</Text>
          <View className="flex-row items-center pt-1">
            <TimeIcon width={16} height={16} />
            <Text className="text-xs text-[#8F90A6]">10 Mins</Text>
          </View>
        </View>
      </View>
      <TouchableOpacity>
        <RemoveItemIcon width={14} height={14} />
      </TouchableOpacity>
    </View>
  )
}
