import ArrowRightIcon from '@/assets/icons/arrowRight.svg'
import OfferIcon from '@/assets/icons/offer.svg'
import { router } from 'expo-router'
import { Text, TouchableOpacity, View } from 'react-native'

export const PromoCode = () => {
  return (
    <View className="flex-row justify-between items-center">
      <View className="flex-row items-center space-x-2">
        <OfferIcon width={24} height={24} />
        <Text className="text-base font-bold">Offers & Promo Code</Text>
      </View>
      <TouchableOpacity
        className="flex-row items-center space-x-0"
        onPress={() => router.push('/discount')}
      >
        <Text className="text-[#6440FE] font-bold">View offers</Text>
        <ArrowRightIcon width={16} height={16} />
      </TouchableOpacity>
    </View>
  )
}
