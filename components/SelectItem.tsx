import { router } from 'expo-router'
import { useState } from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import SelectItemDetail from './SelectItemDetail'

export const SelectItem = () => {
  const [openBottomSheet, setOpenBottomSheet] = useState(false)
  return (
    <>
      <SelectItemDetail openBottomSheet={openBottomSheet} setOpenBottomSheet={setOpenBottomSheet} />
      <TouchableOpacity onPress={() => setOpenBottomSheet(!openBottomSheet)}>
        <View className=" bg-[#6440FE] py-4 px-6 pb-6 flex-row justify-between items-center">
          <View className="flex-row space-x-3 items-center">
            <View className="w-8 h-8 border border-solid border-white rounded-md justify-center items-center">
              <Text className="text-white font-bold">2</Text>
            </View>
            <View>
              <Text className="text-white font-bold text-base">$449</Text>
              <Text className="text-white text-xs">plus texas</Text>
            </View>
          </View>
          <TouchableOpacity onPress={() => router.push('/checkout')}>
            <Text className="text-white text-base font-bold">Continue</Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    </>
  )
}
