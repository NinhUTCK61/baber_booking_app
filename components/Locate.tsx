import { router } from 'expo-router'
import { Text, TouchableOpacity, View } from 'react-native'
import ArrowDownIcon from '../assets/icons/arrowDownIcon.svg'
import ArrowDownLight from '../assets/icons/arrowDownLight.svg'
import LocateIcon from '../assets/icons/locateIcon.svg'
import LocateLight from '../assets/icons/locateLight.svg'

type LocateProps = {
  type?: 'light' | 'dark'
}
export const Locate = (props: LocateProps) => {
  return (
    <TouchableOpacity onPress={() => router.push('/address')}>
      {props.type === 'dark' ? (
        <View className="flex-row items-center gap-x-2">
          <LocateIcon width={13} height={14} />
          <Text className="text-black text-[24] font-bold truncate">Local</Text>
          <ArrowDownIcon width={16} height={16} />
        </View>
      ) : (
        <View className="flex-row items-center gap-x-2 justify-center w-full">
          <LocateLight width={13} height={14} />
          <Text className="text-white text-[24] font-bold truncate">Local</Text>
          <ArrowDownLight width={16} height={16} />
        </View>
      )}
    </TouchableOpacity>
  )
}
