import PlusIcon from '@/assets/icons/plus.svg'
import TimeIcon from '@/assets/icons/time.svg'
import { Image, Text, TouchableOpacity, View } from 'react-native'

export const ShopService = () => {
  return (
    <View className="flex-row items-start justify-between ">
      <View className="flex-row items-start space-x-2">
        <Image
          source={require('@/assets/images/shopService.png')}
          className="h-[75] w-[75] rounded-2xl"
        />
        <View>
          <Text numberOfLines={2} ellipsizeMode="tail" className="text-base font-bold w-[162px]">
            Casmara Brightening Facial
          </Text>
          <Text className="text-xs text-[#8F90A6]">$140</Text>
          <View className="flex-row items-center pt-1">
            <TimeIcon width={16} height={16} />
            <Text className="text-xs text-[#8F90A6]">10 Mins</Text>
          </View>
        </View>
      </View>
      <TouchableOpacity className="flex-row items-center justify-between h-[25] rounded-md shadow-sm bg-white px-2">
        <Text className="text-[#451AFF] pr-1">Select</Text>
        <PlusIcon width={16} height={16} />
      </TouchableOpacity>
    </View>
  )
}
