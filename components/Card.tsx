import DotIcon from '@/assets/icons/dot.svg'
import PercentIcon from '@/assets/icons/percentIcon.svg'
import StarIcon from '@/assets/icons/star.svg'
import { Image, Text, View } from 'react-native'

type CardProps = {
  id: string
  img: string
  for: string
  title: string
  price: string
  service: string
  star: string
  locate: string
  distance: string
  typeEnum?: 'BestOffer'
}

export const Card = (props: CardProps) => {
  return (
    <View className="min-w-min-[290]">
      <View className="relative">
        <Image
          source={require('@/assets/images/salonImage.png')}
          className="min-w-[290] w-full h-[163] rounded-3xl"
        />
        {props.typeEnum === 'BestOffer' && (
          <View className="absolute bottom-4 left-4 flex-row items-center justify-between w-[104] h-[30px] bg-white rounded-full px-3">
            <PercentIcon width={20} height={22} />
            <Text className=" text-[#6440FE] text-xs font-bold">25% Off</Text>
          </View>
        )}
      </View>
      <Text className="text-[#8F90A6] text-xs font-medium mt-1.5">{props.for}</Text>
      <Text numberOfLines={2} ellipsizeMode="tail" className=" text-black text-base font-bold">
        {props.title}
      </Text>
      <View className="flex-row items-center mt-0.5">
        <Text className="text-base text-[#8F90A6]">{props.service}</Text>
        <DotIcon width={4} height={4} className="px-3" />
        <StarIcon width={12} height={12} />
        <Text className="pl-0.5 text-base text-[#8F90A6] font-bold">{props.star}</Text>
      </View>
      <View className="flex-row items-center">
        <Text className="text-base text-[#8F90A6]">{props.locate}</Text>
        <DotIcon width={4} height={4} className="px-2" />
        <Text className="text-base text-[#8F90A6]">{props.distance}</Text>
        <DotIcon width={4} height={4} className="px-3" />
        <Text className="text-base text-[#8F90A6]">{props.price}</Text>
      </View>
    </View>
  )
}
